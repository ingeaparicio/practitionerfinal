const express = require("express");
const router = express.Router();

// Controller
const {
  renderNoteForm,
  createNewNote,
  renderNotes,
  renderEditForm,
  updateNote,
  deleteNote
} = require("../controllers/notes.controller");

// Helper
const { isAuthenticated } = require("../helpers/auth");

// NUEVA NOTA
var numNota=0;
if(true)
{
router.get("/notes/add", isAuthenticated, renderNoteForm);
console.log(numNota)
}
router.post("/notes/new-note", isAuthenticated, createNewNote);

// TRAETE TODAS
router.get("/notes", isAuthenticated, renderNotes);

// EdiTALAS
router.get("/notes/edit/:id", isAuthenticated, renderEditForm);

router.put("/notes/edit-note/:id", isAuthenticated, updateNote);

// BORRALAS
router.delete("/notes/delete/:id", isAuthenticated, deleteNote);

module.exports = router;
